'use strict';

angular.module('iMgr')
.controller(
	'UserInfoCtrl',
	['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope)
		{


    $scope.init  = function(){
      $scope.name = $cookieStore.get("user");
      $scope.email = $cookieStore.get("email");
      $scope.team = $cookieStore.get("teamName");
    } //init

		$scope.changePassword = function(){

		if($scope.password.valid != "undefined"){
			if($scope.password == $scope.passwordRepeat){
				var req =
				{
					method : 'PUT',
					url    : API_CONFIG.API_URL + '/users/' + $cookieStore.get('userId'),
					headers: {
						'X-Parse-Application-Id' : API_CONFIG.APP_ID,
						'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
						'X-Parse-Session-Token'  : $cookieStore.get('session'),
						'Content-Type': 'application/json'
					},
					data: {
						password:$scope.password
						}
				}
				$http(req)
					.success(function (data, status)
					{
						Materialize.toast('Password changed successfully', 3000, 'green darken-3 white-text')
						$scope.password = "";
						$scope.passwordRepeat = "";
						$state.go('login');


				})
				.error(function (data, status) {

					Materialize.toast('Password couldn\'t be changed', 3000, 'red darken-3 white-text')
				})
			}else{
				Materialize.toast('Password don\'t match', 3000, 'red darken-3 white-text')
			}
		}

		}; // changePassword

		}
	]
)
