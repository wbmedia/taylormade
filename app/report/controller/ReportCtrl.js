'use strict';

  angular.module('iMgr')
  .controller('ReportCtrl', ['$scope', 'getTasks',  function($scope, getTasks){
    /*TODO*/
    $scope.taskList = getTasks.data.results;

    $scope.filteredTaskList = [];
    angular.forEach($scope.taskList, function(element) {
      $scope.filteredTaskList.push({
        CourseId: element.CourseId,
        Name: element.Name,
        Address: element.Address,
        City: element.City,
        State: element.State,
        Zip_Code: element.Zip_Code,
        Country: element.Country,
        Course_Type: element.Course_Type,
        status: element.status
        
      
      });
    });

    $('.datepicker').pickadate({
      selectMonths: true, // Creates a dropdown to control month
      selectYears: 15 // Creates a dropdown of 15 years to control year
    });
 

    console.log($scope.taskList);



}]);
