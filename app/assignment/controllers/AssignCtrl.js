'use strict';

angular.module('iMgr')
.controller(
	'AssignCtrl',
	['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope', 'getTeams', 'userInfo',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope, getTeams, userInfo)
		{
      console.log(userInfo);
      $scope.teamList   = getTeams.data.results;
      $scope.selectList = [];
      $scope.hasItems   = false;
      $scope.$watch
      ('teamSelect',
        function(teamID)
        {
          $scope.teamSelected = false;
          if(teamID != undefined)
          {
            var req =
            {
              method : 'GET',
              url    : API_CONFIG.API_URL + '/classes/Team/' + teamID,
              headers: {
                'X-Parse-Application-Id' : API_CONFIG.APP_ID,
                'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
              }
            }
            $http(req)
              .success
              (
                function (data)
                {
                  console.log(data);
                  if(data.usersBelong.length > 0)
                  {
                    $scope.userList     = data.usersBelong;
                    $scope.teamSelected = true;
                  }
                }
              )
          }
        }
      );
      //FUNCTIONS
      $scope.selectMapper = function(mapper){
        $scope.selectedMapper = mapper;
      }
      $scope.selectItem = function(task)
      {
        if(isInArray(task.objectId, $scope.selectList))
        {
          removeA($scope.selectList, task.objectId);
        }else{
          $scope.selectList.push(task.objectId);
        }
        $scope.hasItems = ($scope.selectList.length > 0) ? true : false;
      };
      $scope.assignItems = function(teamId)
      {
        console.log(teamId);
        var batchArray = [];
        angular.forEach
        ($scope.selectList,
          function(value, key)
          {
            var tempObj =
            {
              "method" : "PUT",
              "path"   : "/1/classes/Task/" + value,
              "body"   :
              {
                "assignedTo" : $scope.selectedMapper,
								"team"			 : $scope.team,
                "status"     : "Assigned"
              }
            }
            batchArray.push(tempObj);
          }
        );
        var batch =
        {
          method : 'POST',
          url    :  'https://api.parse.com/1/batch',
          headers:
          {
            'X-Parse-Application-Id' : API_CONFIG.APP_ID,
            'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
          },
          data:
          {
            requests: batchArray
          }
        }
        $http(batch).success
					(function (data)
						{
							console.log(data);
								$scope.find();
								$scope.taskList   = getTasks.data.results;

						}
					)
      };//assign items
		}
	]
)
