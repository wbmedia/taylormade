'use strict';

angular.module('iMgr')
.controller(
	'DetailCtrl',
	['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope','$stateParams', 'getTask',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope, $stateParams, getTask)
		{
			$scope.Name    = getTask.data.Name;
			$scope.Address = getTask.data.Address;
			$scope.city    = getTask.data.City;
			$scope.country = getTask.data.Country;
			$scope.zip     = getTask.data.Zip_Code;
			$scope.typeC   = getTask.data.Course_Type;

			var whereQuery =
			{
				"TaskID" : $stateParams.task_ID
			};

			$scope.getComments = function(){
				var req =
				{
					method : 'GET',
					url    : API_CONFIG.API_URL + '/classes/TaskActivity',
					headers: {
						'X-Parse-Application-Id' : API_CONFIG.APP_ID,
						'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
					},
					params: {
						where: whereQuery
					}
				}
				$http(req)
					.success
					(function (data)
						{
							console.log(data);
							$scope.CommentList = data.results;					
						}
					)
					.error
					(function (data, status) 
						{
							Materialize.toast('Incorrect User Name or Password', 3000, 'red darken-3 white-text')
						}
					)
			}

		}
	]
)
