'use strict';

angular.module('iMgr')
.controller(
	'TasksCtrl',
	['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope', 'getTasks', 'userInfo',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope, getTasks, userInfo)
		{
	    $scope.taskList   = getTasks.data.results;
	    $scope.statusList = [];
	    var currentRoleID = userInfo.data.role;
	    var statusList    =
      [
        {
          "role"   : "Admin",
          "roleID" : "1UAyvvij8d",
          "status" : [
            {
              "objectId" : "7vRiQ3r7a2",
              "name"     : "Closed"
            },
            {
              "objectId" : "RZx57auAdj",
              "name"     : "Checked"
            },
            {
              "objectId" : "ca5UUeju1e",
              "name"     : "Mapped"
            },
            {
              "objectId" : "fRkpEyqHUj",
              "name"     : "Assigned"
            },
            {
              "objectId" : "SBHj8oPisT",
              "name"     : "Imported"
            },
          ]
        },
        {
          "role"   : "QA",
          "roleID" : "GMGjmesDi8",
          "status" : [
            {
              "objectId" : "ca5UUeju1e",
              "name"     : "Mapped"
            },
            {
              "objectId" : "RZx57auAdj",
              "name"     : "Checked"
            },
            {
              "objectId" : "fRkpEyqHUj",
              "name"     : "Assigned"
            }
          ]
        },{
          "role"   : "User",
          "roleID" : "xo58y3iZbF",
          "status" : [
            {
              "objectId" : "fRkpEyqHUj",
              "name"     : "Assigned"
            },
            {
							"objectId" : "ca5UUeju1e",
							"name"     : "Mapped"
            }
          ]
        },
      ];
      ///
      angular.forEach
      (statusList, function(column, key)
	      {
	        if(column.roleID == currentRoleID){
	        	$scope.statusList = column.status;
	        }
	      }
      );
      $scope.confirmStatus = function(task)
      {
      	$state.go
      	('dashboard.my_tasks.confirmation',
      		{
      			'task_ID' : task.objectId,
      			'status'  : task.status
      		}
      	);
      }
		}
	]
)
