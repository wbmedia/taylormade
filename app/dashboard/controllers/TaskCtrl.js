'use strict';

angular.module('iMgr')
.controller
('TaskCtrl',
	['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope', 'getTask', 'userInfo', '$stateParams', '$timeout',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope, getTask, userInfo, $stateParams, $timeout)
		{
      console.log(getTask);
      $scope.task       = getTask.data;
      $scope.activity   = {};
      $scope.hasComment = false;
      var currentRoleID = userInfo.data.role;
      //Listeners
      $scope.$watch
      ('activity.comment',
        function(newVal)
        {
          if(newVal != undefined)
          {
            $scope.hasComment = (newVal.length > 0) ? true : false;
          }
        }
      );
      //Functions
      $scope.setComment = function(task)
      {
        console.log(task);
        var req =
        {
          method : 'POST',
          url    : API_CONFIG.API_URL + '/classes/TaskActivity',
          headers:
          {
            'X-Parse-Application-Id' : API_CONFIG.APP_ID,
            'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
          },
          data :
          {
            'TaskID'     : $stateParams.task_ID,
            'Author'     : task.assignedTo,
            'AuthorName' : $cookieStore.get('user'),
            'Status'     : task.status,
            'Comment'    : $scope.activity.comment
          }
        }
        $http(req)
        .success
        (function (data)
          {
            Materialize.toast('Status Changed', 3000, 'green darken-3 white-text')
            $state.go($state.$current.parent,'',{reload:true});
          }
        )
      }
      $scope.setStatus  = function(task)
      {
        task.status = $stateParams.status;
      	var req =
      	{
      		method : 'PUT',
					url    : API_CONFIG.API_URL + '/classes/Task/' + task.objectId,
					headers:
					{
						'X-Parse-Application-Id' : API_CONFIG.APP_ID,
						'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
					},
					data : task
      	}
      	$http(req)
      	.success
      	(function (data)
	      	{
	      		$scope.setComment(data);
	      	}
      	)
      }
		}
	]
)
