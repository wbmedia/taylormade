'use strict';

angular.module('iMgr')
.controller(
	'DashboardCtrl',
	['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope)
		{
			var wWidth = 0;
			$scope.isAdmin = false;

			$scope.init = function(){
				if ( $cookieStore.get('role') == API_CONFIG.ROLE_ADMIN ){
					$scope.isAdmin = true;
				} else {
					$scope.isAdmin = false;
				}

				$('.mobile-trigger').on('click', function (){
					$('#sidebar ul').toggle();
				});
			}; // init

			$scope.checkMenu = function (){
				wWidth = $(window).width();
				if ( wWidth < 993 ){
					if ( $('#sidebar ul').is(':visible') ){
						$('#sidebar ul').hide();
					}
				}
			};

			// Window Resize
			$(window).resize(function (){
				wWidth = $(window).width();
				if ( wWidth > 992 ){
					if ( $('#sidebar ul').is(':hidden') ){
						$('#sidebar ul').show();
					}
				}
			});

			$scope.logoutUser = function(){
				$cookieStore.remove('session');
				$cookieStore.remove('name');
				$cookieStore.remove('userId');
				$cookieStore.remove('user');
				$cookieStore.remove('avatar');
				$cookieStore.remove('rol');
				$rootScope.userFullName = '';
				$rootScope.userName     = '';
				$rootScope.userAvatar   = '';
				$state.go('login');
			}
		}
	]
)
