'use strict';

angular.module('iMgr')
.controller(
	'LoginCtrl',
	['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope)
		{

			$scope.loginUser = function(){
        var req =
        {
          method : 'GET',
          url    : API_CONFIG.API_URL + '/login',
          headers: {
            'X-Parse-Application-Id' : API_CONFIG.APP_ID,
            'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
          },
          params: $scope.login
        }
        $http(req)
          .success(function (data, status)
          {
            $rootScope.userFullName = data.usu_nombre;
            $rootScope.userName     = data.username;
            $rootScope.userAvatar   = data.avatar;
						$rootScope.email 				= data.email;
						$rootScope.team 				= data.team;

            $cookieStore.put('session'  , data.sessionToken);            
            $cookieStore.put('userId'   , data.objectId);
            $cookieStore.put('user'     , data.username);
            $cookieStore.put('avatar'   , data.avatar);
            $cookieStore.put('email'    , data.email);
            $cookieStore.put('team'     , data.team);
            $cookieStore.put('teamName' , data.teamName);
            
						if(data.role == null){
							alert("User does not have a valid role in the system")
						}else{
							$cookieStore.put('role'	   , data.role);
							$state.go('dashboard.my_tasks',{ 'user_ID': data.objectId });
						}

         })
        .error(function (data, status) {
          Materialize.toast('Incorrect User Name or Password', 3000, 'red darken-3 white-text')
        })
      }; // loginUser

			$scope.recoverPass = function (){
				$state.go("recover");
			}

		}
	]
)
