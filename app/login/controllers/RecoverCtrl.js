'use strict';

angular.module('iMgr')
.controller(
	'RecoverCtrl',
	['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope)
		{

      $scope.resetPass = function(){

        if($scope.recoveryEmail == null){
          Materialize.toast('Email is not valid', 3000, 'red darken-3 white-text')

        }else{
          var req =
          {
            method : 'POST',
            url    : API_CONFIG.API_URL + '/requestPasswordReset',
            headers: {
              'X-Parse-Application-Id' : API_CONFIG.APP_ID,
              'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
              'Content-Type': 'application/json'
            },
            data: {
              email:$scope.recoveryEmail
              }
          }
          $http(req)
            .success(function (data, status)
            {
              Materialize.toast('Password reset request successfully', 3000, 'green darken-3 white-text')
              $scope.password = "";
              $scope.passwordRepeat = "";
              $state.go('login');


          })
          .error(function (data, status) {
            console.log(data);
            if(status == 400){
                if(data.code == 205){
                  Materialize.toast('User not found', 3000, 'red darken-3 white-text')
                }

            }else{
              Materialize.toast('Password couldn\'t be recovery, Something wrong happened', 3000, 'red darken-3 white-text')

            }
          })
          }
        }
				$scope.goBack = function (){
					$state.go("login");
				}
			}


		])
