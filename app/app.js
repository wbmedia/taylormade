'use strict';

var app = angular.module
	('iMgr',
		[
			'ui.router',
			'ui',
			'angular-loading-bar',
			'ngCookies',
			'materializeSelect',
			'materializeTabs',
			'checkAll',
			'pagination',
			'ngSanitize',
			'ngCsv'
		]
	)
app.config
	(
		[
			'$stateProvider',
			'$urlRouterProvider',
			'$locationProvider',
			function ($stateProvider, $urlRouterProvider, $locationProvider)
			{
				$stateProvider
					.state
					('login',
						{
							url : '/login',
							templateUrl: 'app/login/main.html',
							controller: 'LoginCtrl'
						}
					)
					.state
					('recover',
						{
							url : '/recover',
							templateUrl: 'app/login/recover/main.html',
							controller: 'RecoverCtrl'
						}
					)
					.state
					('dashboard',
						{
							url         : '/:user_ID/dashboard',
							templateUrl : 'app/dashboard/main.html',
							controller  : 'DashboardCtrl',
							resolve     :
							{
								userInfo  : function($http, API_CONFIG, $stateParams)
								{
									var req =
									{
										method : 'GET',
										url    : API_CONFIG.API_URL + '/users/' + $stateParams.user_ID,
										headers:
										{
											'X-Parse-Application-Id' : API_CONFIG.APP_ID,
											'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
										}
									}
									return $http(req);
								}
							}
						}
					)
					.state
					('dashboard.my_tasks',
						{
							url         : '/tasks',
							templateUrl : 'app/dashboard/user_tasks.html',
							controller  : 'TasksCtrl',
							resolve     :
							{
								getTasks  : function($http, API_CONFIG, $cookieStore)
								{

									if ($cookieStore.get('role') ==  API_CONFIG.ROLE_ADMIN){
										var whereQuery =
										{
											"status"     : "Checked",
											"status"		 : "Mapped",
											"status"		 : "Assigned"
										};
									}else{
										if ($cookieStore.get('role') ==  API_CONFIG.ROLE_QA){
											var whereQuery =
											{
												"team" : $cookieStore.get('team'),
												"status"     : "Mapped"
											};
										}else{
											var whereQuery =
											{
												"assignedTo" : $cookieStore.get('userId'),
												"status"     : "Assigned"
											};
										}
									}

									var req =
				          {
				            method  : 'GET',
				            url     : API_CONFIG.API_URL + '/classes/Task',
				            headers :
				            {
				              'X-Parse-Application-Id' : API_CONFIG.APP_ID,
				              'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
				            },
				            params:
				            {
				            	where: whereQuery
				            }
				        	};
				      		return $http(req);
								}
							}
						}
					)	.state
						('dashboard.my_history',
							{
								url         : '/History',
								templateUrl : 'app/dashboard/task_history.html',
								controller  : 'HistoryCtrl',
								resolve     :
								{
									getTasks  : function($http, API_CONFIG, $cookieStore)
									{

										if ($cookieStore.get('role') ==  API_CONFIG.ROLE_ADMIN){
											var whereQuery =
											{
												"status"     : "Closed"
											};
										}else{
											if ($cookieStore.get('role') ==  API_CONFIG.ROLE_QA){
												var whereQuery =
												{
													"team" : $cookieStore.get('team'),
													"status"     : "Checked"
												};
											}else{
												var whereQuery =
												{
													"assignedTo" : $cookieStore.get('userId'),
													"status"     : "Mapped"
												};
											}
										}

										var req =
					          {
					            method  : 'GET',
					            url     : API_CONFIG.API_URL + '/classes/Task',
					            headers :
					            {
					              'X-Parse-Application-Id' : API_CONFIG.APP_ID,
					              'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
					            },
					            params:
					            {
					            	where: whereQuery
					            }
					        	};
					      		return $http(req);
									}
								}
							}
						)
					.state
					('dashboard.my_tasks.details',
						{
							url         : '/:task_ID/details',
							templateUrl : 'app/dashboard/task_detail.html',
							controller  : 'DetailCtrl',
							resolve     :
							{
								getTask     : function($http, API_CONFIG, $cookieStore, $stateParams)
								{
									var req =
				          {
				            method  : 'GET',
				            url     : API_CONFIG.API_URL + '/classes/Task/' + $stateParams.task_ID,
				            headers :
				            {
				              'X-Parse-Application-Id' : API_CONFIG.APP_ID,
				              'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
				            }
				        	}
				      		return $http(req);
								}
							}
						}
					)
					.state
					('dashboard.my_tasks.confirmation',
						{
							url         : '/:task_ID/:status/confirmation',
							templateUrl : 'app/dashboard/task_confirmation.html',
							controller  : 'TaskCtrl',
							resolve     :
							{
								getTask     : function($http, API_CONFIG, $cookieStore, $stateParams)
								{
									var req =
				          {
				            method  : 'GET',
				            url     : API_CONFIG.API_URL + '/classes/Task/' + $stateParams.task_ID,
				            headers :
				            {
				              'X-Parse-Application-Id' : API_CONFIG.APP_ID,
				              'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
				            }
				        	}
				      		return $http(req);
								}
							}
						}
					)
					.state
					('dashboard.my_history.confirmation',
						{
							url         : '/:task_ID/:status/confirmation',
							templateUrl : 'app/dashboard/task_confirmation.html',
							controller  : 'TaskCtrl',
							resolve     :
							{
								getTask     : function($http, API_CONFIG, $cookieStore, $stateParams)
								{
									var req =
				          {
				            method  : 'GET',
				            url     : API_CONFIG.API_URL + '/classes/Task/' + $stateParams.task_ID,
				            headers :
				            {
				              'X-Parse-Application-Id' : API_CONFIG.APP_ID,
				              'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
				            }
				        	}
				      		return $http(req);
								}
							}
						}
					)
					.state
					('dashboard.import',
						{
							url         : '/import',
							templateUrl : 'app/import/main.html',
							controller  : 'ImportCtrl'
						}
					)
					.state
					('dashboard.userinfo',
						{
							url         : '/userinfo',
							templateUrl : 'app/userinfo/main.html',
							controller  : 'UserInfoCtrl'
						}
					)
					.state
					('dashboard.assign',
						{
							url         : '/assigment',
							templateUrl : 'app/assignment/main.html',
							controller  : 'AssignCtrl',
							resolve     :
							{
								getTeams: function($http, API_CONFIG)
								{
									var req =
				          {
				            method  : 'GET',
				            url     : API_CONFIG.API_URL + '/classes/Team',
				            headers :
				            {
				              'X-Parse-Application-Id' : API_CONFIG.APP_ID,
				              'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
				            }
				        	};
				      		return $http(req);
								}
							}
						}
					)
					.state
					('dashboard.users',
					  {
				      url         : '/users',
				      templateUrl : 'app/users/main.html',
				      controller  : 'UsersCtrl',
				      resolve     :
				      {
			          getUsers: function ($http, API_CONFIG)
			          {
			          	var query = {teamAssigned : false};
			          	var req   =
		              {
	                  method   : 'GET',
	                  url      : API_CONFIG.API_URL + '/users',
	                  headers  :
	                  {
	                      'X-Parse-Application-Id' : API_CONFIG.APP_ID,
	                      'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
	                  },
	                  params : query
		              };
		              return $http(req);
			          },
			          getTeams: function (TeamService)
			          {
			            return TeamService.all();
			          },
			          getAssigned: function ($http, API_CONFIG)
			          {
			            var req   =
		              {
	                  method   : 'GET',
	                  url      : API_CONFIG.API_URL + '/classes/Team',
	                  headers  :
	                  {
	                      'X-Parse-Application-Id' : API_CONFIG.APP_ID,
	                      'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
	                  }
		              };
		              return $http(req);
			          }
				      }
					  }
					)
					.state
					('dashboard.users.create',
					  {
				      url: '/create',
				      templateUrl: 'app/users/user_create.html',
				      controller: 'UserCtrl',
					  }
					)
					.state
					('dashboard.users.edit',
					  {
				      url: '/:user_ID/edit',
				      templateUrl: 'app/users/user_create.html',
				      controller: 'UserEditCtrl',
				      resolve:
				      {
			          getUser: function($http, API_CONFIG, $stateParams)
			          {
		              var req =
		              {
	                  method: 'GET',
	                  url: API_CONFIG.API_URL + '/users/' + $stateParams.user_ID,
	                  headers: {
                      'X-Parse-Application-Id' : API_CONFIG.APP_ID,
                      'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
	                  }
		              }
		              return $http(req);
			          }
				      }
					  }
          )
          .state
          ('dashboard.report',
          	{
          		url: '/report',
          		templateUrl: 'app/report/main.html',
          		controller: 'ReportCtrl',
              resolve:
              {
                getTasks: function(TaskService) {
                  return TaskService.all();
                }
              }
          	}
          )
				$urlRouterProvider.otherwise('login');
			}
		]
	)

app.run
    (
        [
            '$rootScope',
            '$cookieStore',
            'API_CONFIG',
            '$http',
            '$state',
            function ($rootScope, $cookieStore, API_CONFIG, $http, $state)
            {
                $rootScope.$on
                ('$stateChangeSuccess',
                    function ()
                    {
											if($state.current.name != "recover"){
												$rootScope.checkLogin();

											}
                    }
                )
                $rootScope.closeWin = function()
                {
                	$state.go($state.$current.parent);
                }
                $rootScope.checkLogin = function ()
                {
                    var session =
                    {
                        method : 'GET',
                        url    : API_CONFIG.API_URL + '/sessions/me',
                        headers:
                        {
                            'X-Parse-Application-Id' : API_CONFIG.APP_ID,
                            'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
                            'X-Parse-Session-Token'  : $cookieStore.get('session')
                        }
                    }

                    $http(session)
                        .success(function (data) {
                            if($state.current.name === 'login'){
                            	$state.go('dashboard.my_tasks',{'user_ID': $cookieStore.get('userId') });
                            }
                        })
                        .error(function (data) {
                            $state.go('login');
                        })
                };//checkLogin
            }
        ]
    )

app.constant
	('API_CONFIG',
		{
				API_URL    : 'https://api.parse.com/1',
				APP_ID     : 'kTXDCVscmFiyAij4yRKsl7Y6YGza7wu4ZwImGZxf',
				API_KEY    : 'ZIGdgVXZdilS2Xry50uJELNnOS4w2TMGa1z8nE15',
				ROLE_ADMIN : '1UAyvvij8d',
				ROLE_QA    : 'GMGjmesDi8',
				ROLE_USER  : 'xo58y3iZbF'
		}
	)
