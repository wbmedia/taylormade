'use strict'

angular.module('iMgr')
.factory('TeamService', ['$http', 'API_CONFIG', function ($http, API_CONFIG) {

    return {
        all: function () {
            var req =
            {
                method: 'GET',
                url: API_CONFIG.API_URL + '/classes/Team',
                headers :
                {
                    'X-Parse-Application-Id' : API_CONFIG.APP_ID,
                    'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
                }
            };
            return $http(req);
        }
    };
}]);
