'use strict';

angular.module('iMgr')
.controller(
	'ImportCtrl', 
	['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope', '$timeout',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope, $timeout)
		{      
      $scope.isLoading = false;		

      $scope.importData = function(){       
        //Parse only accepts 50 items per batch so we calculate the number of batch requests we need
        var pages       = Math.ceil($scope.taskList.length / 50);        
        var min         = 0;
        var max         = 0;
        var totalLoaded = 0
        
        for (var i = 0; i < pages; i++) 
        {           
          
          var batchArray = [];  
          min            = i * 50;       
          max            = (i == (pages - 1)) ? $scope.taskList.length : (min + 50);  

          for(var j = min; j < max; j++)
          { 
            $scope.taskList[j].status = "Imported";
            var tempObj = 
            {
              "method" : "POST",
              "path"   : "/1/classes/Task",
              "body"   :              
                $scope.taskList[j]
            }
            batchArray.push(tempObj);
          }

          var batch = 
          {
            method : 'POST',
            url    :  'https://api.parse.com/1/batch',
            headers: 
            { 
              'X-Parse-Application-Id' : API_CONFIG.APP_ID,
              'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
            },
            data: 
            {
              requests: batchArray
            }
          }
          $http(batch)
            .success
            (
              function()
              {
                totalLoaded ++;
                if(i == totalLoaded){
                  Materialize.toast('Data Imported', 3000, 'green darken-3 white-text');
                  $timeout(function(){
                    $scope.isLoaded = false;
                  }, 1500);
                }
              }
            )
        }
      };

			$scope.uploadCSV = function(files)
      { 
         if(/*files[0].type !== "text/csv" ||*/ files[0].name.indexOf("csv") == -1) {
            Materialize.toast('Invalid file type.', 3000, 
                  'red darken-3 white-text')
               return;
         }     

         if(files[0].size <= 0) {
            Materialize.toast('File has no data.', 3000, 
                  'red darken-3 white-text')
               return;
         }
        
        $scope.isLoading = true;
        $scope.isLoaded  = false;
        $scope.taskList  = [];

        $http.post('https://api.parse.com/1/files/' + files[0].name, files[0], 
        {
          withCredentials  : false,
          transformRequest : angular.identity,
          headers          :
          {
            'X-Parse-Application-Id'    : API_CONFIG.APP_ID,
            'X-Parse-REST-API-Key'      : API_CONFIG.API_KEY,
            'Content-Type'              : files[0].type
          }               
        })
        .then(function (data) 
        { 
          var Items = $http.get(data.data.url).then(
            function(response)
            {
              var data = CSVToArray(response.data, ",");            
              if(data[0][0] !== "CourseId" || data[0][1] !== "Name" 
                 || data[0][6] !== "Country") {
                      Materialize.toast('Invalid columns.', 3000, 
                         'red darken-3 white-text');
                      $scope.isLoading = false;
                         return;
              }

              angular.forEach(data, function(column, key) 
              {                
                if(key > 0)
                {
                  var taskObj = {
                    "CourseId"    : column[0],
                    "Name"        : column[1],
                    "Address"     : column[2],
                    "City"        : column[3],
                    "State"       : column[4],
                    "Zip_Code"    : column[5],
                    "Country"     : column[6],
                    "Website"     : column[7],
                    "Course_Type" : column[8]
                  }                 
                  $scope.taskList.push(taskObj);
                  $scope.isLoading = false;
                  $scope.isLoaded  = true;
                }
              });              
            }
          );          
        });                 
      };

		}
	]
)
