'use strict';

angular.module('iMgr')
.controller(
    'UsersCtrl',
    ['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope', 'getUsers', 'getTeams', 'getAssigned',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope, getUsers, getTeams, getAssigned)
        {
            $scope.userList   = getUsers.data.results;
            $scope.teamList   = getTeams.data.results;
            $scope.teamUsers  = getAssigned.data.results;
            $scope.selectList = [];
            $scope.$watch
            ('teamSelect',
              function(obj)
              {
                $scope.teamSelected = obj;                
              }
            );
            $scope.selectItem = function (user) 
            {
                $scope.selectList.push(user);
                console.log($scope.selectList);
            };            

            $scope.openCreateUserModal = function() {
                $state.go('dashboard.users.create');
            };

            $scope.assignItems = function()
            { 
              if(typeof $scope.teamSelected == "string"){
                $scope.teamSelected = JSON.parse($scope.teamSelected);
              }
              var req   =
              {
                method   : 'PUT',
                url      : API_CONFIG.API_URL + '/classes/Team/' + $scope.teamSelected.objectId,
                headers  :
                {
                    'X-Parse-Application-Id' : API_CONFIG.APP_ID,
                    'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
                },
                data :                
                {
                  usersBelong : 
                  {
                    "__op"    : "AddUnique",
                    "objects" : $scope.selectList
                  }
                }
              };
              $http(req)
                .success(function(data){
                  Materialize.toast('Users Assigned', 3000, 'green darken-3 white-text');
                  $scope.getAssigned();
                })
            };//assign teams   
            $scope.removeFrom = function(team, user)
            {
              var req   =
              {
                method   : 'PUT',
                url      : API_CONFIG.API_URL + '/classes/Team/' + team.objectId,
                headers  :
                {
                    'X-Parse-Application-Id' : API_CONFIG.APP_ID,
                    'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
                },
                data :                
                {
                  usersBelong : 
                  {
                    "__op"    : "Remove",
                    "objects" : [user]
                  }
                }
               }
              $http(req)
              .success(function(data){
                $scope.getAssigned();
              })
            };
            $scope.getAssigned = function(){
              var req   =
              {
                method   : 'GET',
                url      : API_CONFIG.API_URL + '/classes/Team',
                headers  :
                {
                    'X-Parse-Application-Id' : API_CONFIG.APP_ID,
                    'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
                }
              };
              $http(req)
                .success(function(data){                  
                  $scope.teamList = data.results;                  
                })
            }         
        }
    ]
)
