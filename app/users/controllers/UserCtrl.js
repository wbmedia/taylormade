'use strict';

angular.module('iMgr')
.controller(
    'UserCtrl',
    ['$scope', '$http', '$state', '$cookieStore', 'API_CONFIG', '$rootScope',
    function ($scope, $http, $state, $cookieStore, API_CONFIG, $rootScope)
        {
            $scope.isAddPage = true;
            $scope.isEditPage = false;
            $scope.createUser = function () {
                var  req =
                {
                    method: 'POST',
                    url: API_CONFIG.API_URL + '/users',
                    headers: {
                        'X-Parse-Application-Id' : API_CONFIG.APP_ID,
                        'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY
                    },
                    data: {
                        'username': $scope.user.username,
                        'password': $scope.user.password,
                        'email': $scope.user.email,
                        'name': $scope.user.name,
                        'role' : $scope.userTypeValue
                    }
                }
                $http(req)
                    .success(function(data){
                        $rootScope.closeWin();
                        $scope.userList.push($scope.user);
                    })
                    .error(function(error){
                        Materialize.toast(error.data.results);
                    })

            };
        }
    ]
)
