angular.module('iMgr')
.controller('UserEditCtrl', ['$scope', '$location', '$http', 'API_CONFIG', '$cookieStore', 'getUser', function($scope, $location, $http, API_CONFIG, $cookieStore, getUser){

    $scope.isAddPage = false;
    $scope.isEditPage = true;
    /* TODO */
    $scope.$watch('getUser', function(){
        $scope.user = {
            objectId : getUser.data.objectId,
            userTypeValue: getUser.data.role,
            name: getUser.data.name,
            email: getUser.data.email,
            username: getUser.data.username,
            password: getUser.data.password
        };
    });

    $scope.checkRole = function(role) {
        return $scope.user.userTypeValue === role;
    }

    $scope.editUser = function(user){
        var userObjId = $scope.user.objectId;
        console.log('user session', $cookieStore.get('session'));
        console.log(user);
        console.log('obj id', userObjId);
        var req =
        {
            method: 'PUT',
            url: API_CONFIG.API_URL + '/users/' + userObjId,
            headers:
            {
                'X-Parse-Application-Id' : API_CONFIG.APP_ID,
                'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
                'X-Parse-Session-Token'  : $cookieStore.get('session'),
                'Content-Type': 'application/json'
            },
            data: {
                user: user
            }
        }
        $http(req)
        .success(function(data) {
            Materialize.toast('User: ' + $scope.user.username + ' updated.');
            $timeout(function(){
              $state.go($state.$current.parent);
            },3200);
        });
    }

    console.log(getUser.data.name);
    console.log(getUser.data.email);
    console.log(getUser.data.username);
    console.log(getUser.data.password);
    console.log(getUser.data.name);
    console.log(getUser.data.role)
    console.log(getUser.data.objectId);


}])
