'use strict';

angular.module('materializeTabs', [])
	.directive('materializeTabs', ['$timeout', function ($timeout) {		
		return {
			restrict: 'A',
			link: function (scope, iElement, iAttrs) {				
				$timeout(function(){
					(iElement).tabs();
				},500)
			}
		};
	}])