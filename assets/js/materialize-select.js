'use strict';

angular.module('materializeSelect', [])
	.directive('materializeSelect', ['$timeout', function ($timeout) {
		return {
			restrict: 'A',
			link: function (scope, iElement, iAttrs) {
				$timeout(function(){
					(iElement).material_select();
				},500)
			}
		};
	}])