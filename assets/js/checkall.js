'use strict';

angular.module('checkAll', [])
	.directive('checkAll', ['$timeout', function ($timeout) {		
		return {
			restrict: 'A',
			link: function (scope, iElement, iAttrs) {								
				(iElement).click(function(){
					$('input[type="checkbox"].checkbox').trigger('click');
				});
			}
		};
	}])