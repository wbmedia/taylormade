'use strict';

angular.module('pagination', [])
	.directive('pagination', function(API_CONFIG) 
	{
		return{
				restrict : 'EA',
				require  : ['pagination', '^ngPage'],
				scope    : 
				{	
					tabla  : '@',
					ngPage : '='
				},
			template   : '<ul class="pagination"><li class="waves-effect" ng-class="{active: currentPage == $index+1}" ng-repeat="i in getNumber(pages) track by $index" ng-click="pageChange($index+1)"><a><span>{{$index+1}}</span></a</li></ul>',
			replace    : true,
			controller : ['$scope', '$http', function($scope, $http) {
				$scope.Math = window.Math;
				
				var itemsPerPage = 10;

				$scope.getPages    = function(query){
					$scope.currentQuery = query
					var pag          = 
					{
						method  : 'GET',
						url     : 'https://api.parse.com/1/' + $scope.tabla,
						headers : 
						{
							'X-Parse-Application-Id' : API_CONFIG.APP_ID,
							'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
						},
						params  :
						{

							where: $scope.currentQuery,
							count:'1',
							limit:'0'
						}
					};

					$http(pag)
						.success(function (data) 
						{
							$scope.pages 	= Math.ceil(data.count/itemsPerPage);
							if ($scope.currentPage === undefined) 
							{
								$scope.pageChange("1");
							};		            	
						});
				};

				$scope.getNumber 	= function(num) 
				{
					return new Array(num);   
				};

				$scope.pageChange = function(page) 
				{
						if (page === undefined) {
							$scope.currentPage = '1';
						}
						else
						{
							$scope.currentPage = page;
						}

					$scope.currentSkip     = Math.ceil(itemsPerPage*($scope.currentPage-1));
					var pag        = {
						method  : 'GET',
						url     : 'https://api.parse.com/1/' + $scope.tabla,
						headers : {
							'X-Parse-Application-Id' : API_CONFIG.APP_ID,
							'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
						},
						params  :{
							where : $scope.currentQuery,
							limit : itemsPerPage,
							skip  : $scope.currentSkip,
							order : $scope.currentFilter
						}
					};

					$http(pag)
						.success(function (data) {
							$scope.$parent.dataGrid = data.results;
						});
				};

				$scope.$parent.filter = function(sortBy)
				{
					if ($scope.currentFilter == sortBy)
				 	{
						$scope.currentFilter	=	"-"+sortBy
					}
					else
					{
						$scope.currentFilter	=	sortBy
					}
						$scope.currentSkip    = Math.ceil(itemsPerPage*($scope.currentPage-1));
					var pag        = 
					{
					method  : 'GET',
					url     : 'https://api.parse.com/1/' + $scope.tabla,
					headers : 
					{
						'X-Parse-Application-Id' : API_CONFIG.APP_ID,
						'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
					},
					params  :
					{
						where : $scope.currentQuery,
						limit : itemsPerPage,
						skip  : $scope.currentSkip,
						order : $scope.currentFilter
					}
					};

					$http(pag)
						.success(function (data)
						{
							$scope.$parent.dataGrid = data.results;
							            	
						});	      		
				};

				$scope.$parent.find = function(searchBy)
				{
					var whereQuery;

						if (searchBy === undefined) 
						{
							$scope.currentQuery = {"status":"Imported"};
							$scope.currentPage      = '1';

							$scope.currentSkip = Math.ceil(itemsPerPage*($scope.currentPage - 1));
							var pag        = {
								method  : 'GET',
								url     : 'https://api.parse.com/1/' + $scope.tabla,
								headers : {
									'X-Parse-Application-Id' : API_CONFIG.APP_ID,
									'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
								},
								params  :{
									where : $scope.currentQuery,
									limit : itemsPerPage,
									skip  : $scope.currentSkip,
									order : $scope.currentFilter
								}
							};

							$http(pag)
								.success(function (data) {
									$scope.$parent.dataGrid = data.results;
								});
					 }
					else{
						if (searchBy.columna == "") {
							$scope.currentQuery = {"status":"Imported"};
						};
						$scope.currentPage      = '1';
						$scope.currentQuery =	'{"activo": true, ' + '"'+ searchBy.columna + '"' + ':' + '"' +searchBy.value + '"'+ '}'

					}
					
					var pag        	= 
					{
						method  : 'GET',
						url     : 'https://api.parse.com/1/' + $scope.tabla,
						headers : 
						{
							'X-Parse-Application-Id' : API_CONFIG.APP_ID,
							'X-Parse-REST-API-Key'   : API_CONFIG.API_KEY,
						},
						params  :
						{
							where : $scope.currentQuery,
							limit : itemsPerPage,
							order : $scope.currentFilter
						}
					};

					$http(pag)
						.success(function (data)
						{
							$scope.getPages($scope.currentQuery);
							$scope.$parent.dataGrid = data.results;
							            	
						});	     
				};
					$scope.getPages('{"status":"Imported"}');
			}]
		}
	})