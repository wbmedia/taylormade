# Taylormade Tasks #

![TaylorMade-Logo.jpg](https://bitbucket.org/repo/54Arrd/images/1721828882-TaylorMade-Logo.jpg)

Web app para dar seguimiento al proceso de mapeo de campos de golf de Taylormade

### How do I get set up? ###

1. Get the code: 

```
#!
https://gitlab.com/wbmedia/taylormade.git
```

2. Run a http server: 

```
#!
$ node server
```

3. Open in a browser http://localhost:8000